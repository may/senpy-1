use serde_json::Value;

use crate::parser::Node;
use std::{collections::HashMap, fs, io::Write, process};

const TEMP_PATH: &str = "/tmp/pytemplater";

pub fn evaluate(text: &str) -> String {
    let mut file = fs::File::create(TEMP_PATH).unwrap();
    write!(file, "{}", text).unwrap();

    let output = process::Command::new("python")
        .arg(TEMP_PATH)
        .output()
        .unwrap();

    String::from_utf8_lossy(&output.stdout).trim().to_string()
}

pub fn insert_variables(text: &mut String, variables: &HashMap<String, Node>) {
    for (key, value) in variables.iter() {
        text.push_str(&format!("\n{} = {}\n", key, value.pyrepr()));
    }
}

pub fn resolve(text: &str, variables: HashMap<String, Node>) -> String {
    let mut code = String::new();
    for (key, value) in variables.iter() {
        code.push_str(&format!(
            "\n{} = {}\n",
            key,
            value.pyrepr().replace("\n", "\\n")
        ));
    }
    code.push_str(&format!("\nprint(f\"{}\")\n", text));
    evaluate(&code)
}

pub fn execute_python(text: &str, variables: &mut HashMap<String, Node>) {
    let mut whitespace = 0;
    'counter: for chr in text.chars() {
        if matches!(chr, '\t' | '\n' | ' ') {
            whitespace += 1;
        } else {
            break 'counter;
        }
    }

    let text = text
        .split('\n')
        .filter(|x| !x.trim().is_empty())
        .map(|x| {
            if whitespace != 0 {
                &x[whitespace - 1..x.len()]
            } else {
                x
            }
        })
        .collect::<Vec<_>>()
        .join("\n");

    let mut builder = String::new();
    insert_variables(&mut builder, variables);
    builder.push_str(&text);
    builder.push_str(
        "\nvars = {key: value for key, value in locals().items() if not key.startswith('__') and type(value) in [int, float, str, bool]}\n",
    );
    builder.push_str("\nimport json\n");
    builder.push_str("\nprint(json.dumps(vars))\n");

    let result = evaluate(&builder);
    let data: Value = serde_json::from_str(&result).unwrap();
    if let Value::Object(data) = data {
        for (key, value) in data.iter() {
            let data = extract(value);
            variables.insert(key.clone(), data);
        }
    }
}

pub fn extract(data: &Value) -> Node {
    if let Value::String(text) = data {
        Node::String(text.clone())
    } else if let Value::Number(num) = data {
        if let Some(num_i64) = num.as_i64() {
            Node::Integer(num_i64)
        } else if let Some(num_f64) = num.as_f64() {
            Node::Float(num_f64)
        } else {
            panic!("Not possible")
        }
    } else if let Value::Bool(value) = data {
        Node::Bool(*value)
    } else {
        panic!("u messed up")
    }
}

pub fn extract_value(text: &str) -> Node {
    let data: Value = serde_json::from_str(text).unwrap();
    extract(&data)
}
