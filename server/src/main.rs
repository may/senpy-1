use std::io::Write;

mod lexer;
mod parser;
mod runtime;
mod template;

fn input(prompt: &str) -> String {
    print!("{}", prompt);
    std::io::stdout().flush().unwrap();
    let mut temp = String::new();
    std::io::stdin().read_line(&mut temp).unwrap();
    temp.trim().to_string()
}

fn main() {
    let text = include_str!("../../showcase.sps");
    let tokens = lexer::lex(text);
    let ast = parser::parse(tokens);

    let mut runtime = runtime::Runtime::new(ast);
    'main: loop {
        let data = input(" > ");
        let response = runtime.poll(data);
        if let Some(event) = response {
            println!("{:#?}", event);
        } else {
            break 'main;
        }
    }
}
