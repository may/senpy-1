use std::collections::HashMap;

use crate::parser::{Node, Operation};

pub struct Runtime {
    ast: Vec<Node>,
    pc: usize,
    stack: Vec<Node>,
    functions: HashMap<String, Node>,
    call_stack: Vec<(Vec<Node>, usize)>,
    options: Vec<Vec<Node>>,
    variables: HashMap<String, Node>,
    case_stack: Vec<(Vec<Node>, bool)>,
}

#[derive(Debug)]
pub enum OutputEvent {
    Text(Vec<String>),
    Menu(Vec<String>),
    Input,
}

impl Runtime {
    pub fn new(ast: Vec<Node>) -> Self {
        Self {
            ast,
            pc: 0,
            stack: vec![],
            call_stack: vec![],
            functions: HashMap::new(),
            options: vec![],
            variables: HashMap::new(),
            case_stack: vec![],
        }
    }

    fn jump_to(&mut self, block: Node) {
        if let Node::Block(block) = block {
            self.call_stack.push((self.ast.clone(), self.pc));
            self.ast = block;
            self.pc = 0;
        }
    }

    fn evaluate_array(&mut self, array: Vec<Node>) -> Vec<String> {
        array
            .iter()
            .map(|x| {
                if let Node::String(line) = x {
                    line.clone()
                } else {
                    "".to_string()
                }
            })
            .collect::<Vec<_>>()
    }

    fn execute_operator(&mut self, operator: Operation, data: String) -> Option<OutputEvent> {
        match operator {
            Operation::Display => {
                let text = self.stack.pop();
                let speaker = self.stack.pop();
                let mut response = if let (Some(Node::String(text)), Some(Node::String(speaker))) =
                    (text.clone(), speaker.clone())
                {
                    Some(OutputEvent::Text(vec![speaker, text]))
                } else if let (Some(Node::Block(lines)), Some(Node::String(speaker))) =
                    (text.clone(), speaker.clone())
                {
                    let mut parameters = vec![speaker];
                    let mut lines = self.evaluate_array(lines);
                    parameters.append(&mut lines);
                    Some(OutputEvent::Text(parameters))
                } else {
                    println!("Bad arguments to Display");
                    None
                };
                if let Some(OutputEvent::Text(lines)) = response {
                    let new_lines = lines
                        .iter()
                        .map(|x| crate::template::resolve(x, self.variables.clone()))
                        .collect::<Vec<_>>();
                    response = Some(OutputEvent::Text(new_lines));
                }
                response
            }
            Operation::Define => {
                let name = self.stack.pop();
                let func = self.stack.pop();
                if let (Some(Node::String(name)), Some(func)) = (name, func) {
                    self.functions.insert(name, func);
                }
                None
            }
            Operation::Call => {
                let name = self.stack.pop();
                if let Some(Node::String(name)) = name {
                    let block = self.functions.get(&name).unwrap();
                    self.jump_to(block.clone());
                }
                None
            }
            Operation::Menu => {
                let options = self.stack.pop();
                if let Some(Node::Block(options)) = options {
                    let options = self.evaluate_array(options);
                    self.options.clear();
                    Some(OutputEvent::Menu(options))
                } else {
                    None
                }
            }
            Operation::Get => {
                let value = Node::String(data);
                self.stack.push(value);
                None
            }
            Operation::Option => {
                let block = self.stack.pop();
                if let Some(Node::Block(nodes)) = block {
                    self.options.push(nodes);
                }
                None
            }
            Operation::Select => {
                let index = self.stack.pop();
                if let Some(Node::String(index)) = index {
                    let idx: usize = index.parse().unwrap();
                    let option = self.options[idx].clone();
                    self.jump_to(Node::Block(option));
                }
                None
            }
            Operation::Input => Some(OutputEvent::Input),
            Operation::Store => {
                let key = self.stack.pop();
                let value = self.stack.pop();
                if let (Some(Node::String(key)), Some(value)) = (key, value) {
                    self.variables.insert(key, value);
                }
                None
            }
            Operation::Execute => {
                let code = self.stack.pop();
                if let Some(Node::Code(text)) = code {
                    crate::template::execute_python(&text, &mut self.variables);
                }
                None
            }
            Operation::Case => {
                let condition = self.stack.pop();
                let function = self.stack.pop();
                if let (Some(Node::Code(text)), Some(Node::Block(nodes))) = (condition, function) {
                    let mut builder = String::new();
                    builder.push_str("\nimport json\n");
                    crate::template::insert_variables(&mut builder, &self.variables);
                    builder.push_str(&format!("print(json.dumps({}))", text));
                    let result = crate::template::evaluate(&builder);
                    let value = crate::template::extract_value(&result);
                    if let Node::Bool(data) = value {
                        self.case_stack.push((nodes, data));
                    }
                }
                None
            }
            Operation::Switch => {
                'switch: for (function, condition) in self.case_stack.clone() {
                    if condition {
                        self.case_stack.clear();
                        self.jump_to(Node::Block(function));
                        break 'switch;
                    }
                }
                None
            }
        }
    }
    fn execute_instruction(&mut self, instruction: Node, data: String) -> Option<OutputEvent> {
        match instruction {
            Node::String(text) => {
                self.stack.push(Node::String(text));
                None
            }
            Node::Code(text) => {
                self.stack.push(Node::Code(text));
                None
            }
            Node::Operator(op) => self.execute_operator(op, data),
            Node::Block(nodes) => {
                self.stack.push(Node::Block(nodes));
                None
            }
            _ => panic!("How did you do this??"),
        }
    }
    pub fn poll(&mut self, data: String) -> Option<OutputEvent> {
        loop {
            // Pop from stack frame if end is reached, to see if
            // in a function call. Repeat until call stack is empty,
            // meaning execution is complete
            for _ in 0..self.call_stack.len() + 1 {
                if self.pc >= self.ast.len() {
                    if let Some(frame) = self.call_stack.pop() {
                        (self.ast, self.pc) = frame;
                    } else {
                        return None;
                    }
                }
            }

            let instruction = self.ast[self.pc].clone();
            self.pc += 1;
            if let Some(response) = self.execute_instruction(instruction, data.clone()) {
                return Some(response);
            }
        }
    }
}
