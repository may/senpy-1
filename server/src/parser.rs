use crate::lexer::Token;

#[derive(Debug, Clone)]
pub enum Operation {
    Display,
    Call,
    Define,
    Menu,
    Select,
    Option,
    Get,
    Input,
    Store,
    Execute,
    Case,
    Switch,
}

#[derive(Debug, Clone)]
pub enum Node {
    String(String),
    Code(String),
    Operator(Operation),
    Block(Vec<Node>),
    Integer(i64),
    Float(f64),
    Bool(bool),
}

impl Node {
    pub fn pyrepr(&self) -> String {
        match self {
            Node::String(text) => format!("\"{}\"", text),
            Node::Integer(num) => format!("{}", num),
            Node::Float(num) => format!("{}", num),
            Node::Bool(value) => {
                if *value {
                    "True".to_string()
                } else {
                    "False".to_string()
                }
            }
            _ => panic!("Pyrepr failed"),
        }
    }
}

impl Operation {
    fn parse(op: &str) -> Operation {
        match op {
            "." => Operation::Display,
            "call" => Operation::Call,
            "def" => Operation::Define,
            "menu" => Operation::Menu,
            "select" => Operation::Select,
            "get" => Operation::Get,
            "|" => Operation::Option,
            "input" => Operation::Input,
            "store" => Operation::Store,
            "x" => Operation::Execute,
            "@" | "case" => Operation::Case,
            "switch" => Operation::Switch,
            _ => panic!("Unknown op: {}", op),
        }
    }
}

pub fn parse(tokens: Vec<Token>) -> Vec<Node> {
    let mut result = vec![];
    let mut builder = vec![];
    let mut level = 0;

    for token in tokens {
        match token {
            Token::OpenBracket => {
                level += 1;
                if level != 1 {
                    builder.push(token);
                }
            }
            Token::CloseBracket => {
                level -= 1;
                if level == 0 {
                    let inner_node = parse(builder.clone());
                    result.push(Node::Block(inner_node));
                    builder.clear();
                } else {
                    builder.push(token);
                }
            }
            Token::String(ref text) => {
                if level == 0 {
                    result.push(Node::String(text.to_string()));
                } else {
                    builder.push(token);
                }
            }
            Token::Code(ref text) => {
                if level == 0 {
                    result.push(Node::Code(text.to_string()));
                } else {
                    builder.push(token);
                }
            }
            Token::Operator(ref operator) => {
                if level == 0 {
                    let op = Operation::parse(operator);
                    result.push(Node::Operator(op));
                } else {
                    builder.push(token);
                }
            }
        }
    }

    result
}
