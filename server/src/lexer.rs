#[derive(Debug, Clone)]
pub enum Token {
    String(String),
    Code(String),
    Operator(String),
    OpenBracket,
    CloseBracket,
}

pub fn lex(text: &str) -> Vec<Token> {
    let mut tokens = vec![];
    let mut chars = text.chars();

    while let Some(chr) = chars.next() {
        match chr {
            '[' => tokens.push(Token::OpenBracket),
            ']' => tokens.push(Token::CloseBracket),
            ' ' | '\t' | '\n' => {}
            '"' => {
                let mut result = String::new();
                let mut next_char = chars.next().unwrap();
                let mut previous_char = chr;
                while next_char != '"' || previous_char == '\\' {
                    if next_char != '\\' || previous_char == '\\' {
                        result.push(next_char);
                        previous_char = ' ';
                    } else {
                        previous_char = next_char;
                    }
                    next_char = chars.next().unwrap();
                }
                tokens.push(Token::String(result));
            }
            '`' => {
                let mut result = String::new();
                let mut next_char = chars.next().unwrap();
                while next_char != chr {
                    result.push(next_char);
                    next_char = chars.next().unwrap();
                }
                tokens.push(Token::Code(result));
            }
            _ => {
                let mut result = String::new();
                let mut next_char = chr;
                while !matches!(next_char, '\t' | '\n' | ' ') {
                    result.push(next_char);
                    next_char = chars.next().unwrap();
                }
                tokens.push(Token::Operator(result));
            }
        }
    }

    tokens
}
